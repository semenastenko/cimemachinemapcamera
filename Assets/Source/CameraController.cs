using Cinemachine;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(CinemachineVirtualCamera))]
public class CameraController : MonoBehaviour, CameraInput.IMovementActions
{
    private Camera _camera;

    private Transform _followTarget;

    private CinemachineFramingTransposer _transposer;

    private CinemachinePOV _pov;

    private Plane _plane;

    private Vector3 _dragOrigin;

    [SerializeField]
    public float _minZoom = 1;

    [SerializeField]
    public float _maxZoom = 150;

    private void Awake()
    {
        _camera = Camera.main;
        _plane = new Plane(Vector3.up, new Vector3(0, 0, 0));

        var vcam = GetComponent<CinemachineVirtualCamera>();

        _followTarget = vcam.Follow;
        Debug.Assert(_followTarget != null);

        _transposer = vcam.GetCinemachineComponent<CinemachineFramingTransposer>();
        Debug.Assert(_transposer != null);

        _pov = vcam.GetCinemachineComponent<CinemachinePOV>();
        Debug.Assert(_pov != null);

        var input = new CameraInput();
        input.Movement.SetCallbacks(this);
        input.Enable();
    }

    public void OnDrag(InputAction.CallbackContext context)
    {
        var mousePosition = context.ReadValue<Vector2>();
        Vector3 drag = Vector3.zero;

        if (CastToPlane(mousePosition, out var position))
        {
            if (context.phase == InputActionPhase.Started)
            {
                _dragOrigin = position;
            }
            if (context.phase == InputActionPhase.Performed)
            {
                drag = _dragOrigin - position;
            }
        }

        if (drag.sqrMagnitude > 0)
        {
            drag.y = 0;
            _followTarget.Translate(drag);
        }
    }

    public void OnRotate(InputAction.CallbackContext context)
    {
        var axis = context.ReadValue<Vector2>();

        if (axis.sqrMagnitude > 0)
        {
            axis *= Time.deltaTime;
            _pov.m_HorizontalAxis.Value += axis.x;
            _pov.m_VerticalAxis.Value -= axis.y;
        }
    }

    public void OnZoom(InputAction.CallbackContext context)
    {
        var zoom = context.ReadValue<float>();

        var distance = _transposer.m_CameraDistance;

        zoom *= Time.deltaTime;
        distance -= zoom;
        distance = Mathf.Clamp(distance, _minZoom, _maxZoom);

        _transposer.m_CameraDistance = distance;
    }

    private bool CastToPlane(Vector3 mousePosition, out Vector3 worldPosition)
    {
        Ray ray = _camera.ScreenPointToRay(mousePosition);

        if (_plane.Raycast(ray, out float distance))
        {
            worldPosition = ray.GetPoint(distance);

            return true;
        }

        worldPosition = Vector3.zero;

        return false;
    }
}
